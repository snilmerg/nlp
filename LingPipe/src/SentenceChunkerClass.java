import com.aliasi.chunk.*;
import com.aliasi.sentences.*;
import com.aliasi.tokenizer.*;
import java.util.*;

public class SentenceChunkerClass {

    static String text = "When determining the end of sentences we need to consider several factors. Sentences may end with exclamation marks! Or possibly questions marks? Within sentences we may find numbers like 3.14159, abbreviations such as found in Mr. Smith, and possibly ellipses either within a sentence …, or at the end of a sentence….";
    static String text2 = "“Will the prime minister listen to the will of house last night, end this costly charade, and rule out no deal?” the Labour leader asked. May responded by saying Corbyn was himself ruling out the only deal acceptable to Brussels. “The only way to avoid no deal is to vote for the deal,” she said.";

    public static void main(String[] args) {

        TokenizerFactory tokenizerfactory = IndoEuropeanTokenizerFactory.INSTANCE;
        SentenceModel sentenceModel = new IndoEuropeanSentenceModel();

        SentenceChunker sentenceChunker = new SentenceChunker(tokenizerfactory, sentenceModel);

        Chunking chunking = sentenceChunker.chunk(text2.toCharArray(),0, text2.length());

        Set<Chunk> sentences = chunking.chunkSet();
        String slice = chunking.charSequence().toString();

        for (Chunk sentence : sentences) {
            System.out.println("[" + slice.substring(sentence.start(), sentence.end()) + "]");
        }
    }
}