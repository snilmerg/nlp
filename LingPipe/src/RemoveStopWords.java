import com.aliasi.tokenizer.*;
// usuwanie stopwords
public class RemoveStopWords {

    static String text = "Amy didn�t like it. There were paintings of zombies and skeletons on the walls. We�re going to take photos for the school art competition, said Kamal.";

    public static void main(String[] args) {

        TokenizerFactory factory = IndoEuropeanTokenizerFactory.INSTANCE;
        factory = new EnglishStopTokenizerFactory(factory);
        Tokenizer tokenizer = factory.tokenizer(text.toCharArray(), 0, text.length());
        for (String token : tokenizer)
        {
	        System.out.println(token);
        }

    }
}