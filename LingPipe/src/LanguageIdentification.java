import java.io.*;

import com.aliasi.classify.*;
import com.aliasi.util.*;
// identyfikacja języka na podstawie modelu
public class LanguageIdentification {
	
	public static void main(String[] args) {
		String text = "An overly sentimental film with a somewhat problematic message, but its sweetness and charm are occasionally enough to approximate true depth and grace.";
		String text2 = "Svenska är ett östnordiskt språk som talas av drygt tio miljoner personer främst i Sverige där språket har en dominant ställning som huvudspråk, men även som det ena nationalspråket i Finland och som enda officiella språk på Åland.";
		
		LMClassifier classifier = null;
		try
		{
			classifier = (LMClassifier) AbstractExternalizable.readObject(new File("models", "langid-leipzig.classifier"));
		}
		catch (IOException | ClassNotFoundException ex)
		{
			System.out.print("File not found.");
			return;
		}
		
		JointClassification classification = classifier.classify(text);
		System.out.println("Text: " + text);
		String bestCategory = classification.bestCategory();
		System.out.println("Best category: " + bestCategory);
		
		System.out.println();
		classification = classifier.classify(text2);
		System.out.println("Text: " + text2);
		bestCategory = classification.bestCategory();
		System.out.println("Best category: " + bestCategory);
	}
}