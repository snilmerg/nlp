import java.util.regex.*;
// znajdowanie numeru telefonu wykorzystując wyrażenia regularne
public class PhoneNumberFinder {

	public static void main(String[] args) {
		String phoneNumberRE = "\\d{3}-\\d{3}-\\d{3}";
		Pattern pattern = Pattern.compile(phoneNumberRE);
		Matcher matcher = pattern.matcher("123-456-789 012345");
		while(matcher.find())
		{
			System.out.println(matcher.group() + " [" + matcher.start() + ":" + matcher.end() + "]");
		}
	}

}