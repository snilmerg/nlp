import java.io.*;

import com.aliasi.classify.*;
import com.aliasi.util.*;
// klasyfikacja do 4 kategorii zdefiniowanych w modelu
public class ClassificationCategories {
	
	public static void main(String[] args) {
		String forSale = "Finding a home for sale has never been easier. With Homes.com, you can search new homes, foreclosures, multi-family homes, as well as condos and townhouses for sale. You can even search our real estate agent directory to work with a proffesional Realtor and find your perfect home.";
		String martinLuther = "Luther taught that salvation and subsequently eternity in heaven is not earned by good deeds but is received only as the free gift of God's grace through faith in Jesus Christ as redeemer from sin and subsequently eternity in Hell.";
		
		LMClassifier classifier = null;
		try
		{
			classifier = (LMClassifier) AbstractExternalizable.readObject(new File("models", "classifier.model"));
		}
		catch (IOException | ClassNotFoundException ex)
		{
			System.out.print("File not found.");
			return;
		}
		
		JointClassification classification = classifier.classify(forSale);
		System.out.println("Text: " + forSale);
		String bestCategory = classification.bestCategory();
		System.out.println("Best category: " + bestCategory);
		
		System.out.println();
		classification = classifier.classify(martinLuther);
		System.out.println("Text: " + martinLuther);
		bestCategory = classification.bestCategory();
		System.out.println("Best category: " + bestCategory);
	}
}