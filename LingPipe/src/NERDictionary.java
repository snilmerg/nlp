import java.util.*;

import com.aliasi.chunk.*;
import com.aliasi.dict.*;
import com.aliasi.tokenizer.*;
// znajdowanie byt�w nazwanych wykorzystuj�c zdefiniowany s�ownik
public class NERDictionary {
	
	public static MapDictionary<String> dictionary;
	
	public static void initializeDictionary()
	{
		dictionary = new MapDictionary<String>();
		dictionary.addEntry(new DictionaryEntry<String>("Joe", "PERSON", 1.0));
		dictionary.addEntry(new DictionaryEntry<String>("Fred", "PERSON", 1.0));
		dictionary.addEntry(new DictionaryEntry<String>("Boston", "PLACE", 1.0));
		dictionary.addEntry(new DictionaryEntry<String>("pub", "PLACE", 1.0));
		dictionary.addEntry(new DictionaryEntry<String>("Vermont", "PLACE", 1.0));
		dictionary.addEntry(new DictionaryEntry<String>("IBM", "ORGANIZATION", 1.0));
		dictionary.addEntry(new DictionaryEntry<String>("Sally", "PERSON", 1.0));
	}
	
	public static void displayChunkSet(ExactDictionaryChunker chunker, String text)
	{
		Chunking chunking = chunker.chunk(text);
		Set<Chunk> set = chunking.chunkSet();
		for (Chunk chunk : set)
		{
			System.out.println("Type: " + chunk.type() + " Entity: [" + text.substring(chunk.start(), chunk.end()) + "] Score " + chunk.score());
		}
	}
	
	public static void main(String[] args) {
		initializeDictionary();
		ExactDictionaryChunker dictionaryChunker = new ExactDictionaryChunker(
			dictionary, IndoEuropeanTokenizerFactory.INSTANCE, true, false);
		String sentences[] = {"Joe was the last person to see Fred. ", "He saw him in Boston at McKenzie's pub at 3:00 where he paid $2.45 for an ale. ", "Joe wanted to go to Vermont for the day to visit a cousin who works at IBM, but Sally and he had to look for Fred."};
		
		for (String sentence : sentences)
		{
			System.out.println("\nTEXT=" + sentence);
			displayChunkSet(dictionaryChunker, sentence);
		}
	}

}