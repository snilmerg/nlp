import com.aliasi.tokenizer.*;

public class Stemming {

    public static void main(String[] args) {

        String words[] = {"bank", "banking", "banks", "banker", "banked", "bankart"};
        TokenizerFactory factory = IndoEuropeanTokenizerFactory.INSTANCE;
        factory = new PorterStemmerTokenizerFactory(factory);
        String stems[] = new String[words.length];
        for (String word : words)
        {
            Tokenization tokenizer = new Tokenization(word, factory);
            stems = tokenizer.tokens();
            System.out.print("Word: " + word);
            for (String stem : stems)
            {
                System.out.println(" Stem: " + stem);
            }
        }

    }
}