import java.io.*;
import java.util.*;

import com.aliasi.chunk.*;
import com.aliasi.util.AbstractExternalizable;
// znajdowanie byt�w nazwanych wykorzystuj�c model
public class NERModel {
	
	public static void displayChunkSet(AbstractCharLmRescoringChunker chunker, String text)
	{
		Chunking chunking = chunker.chunk(text);
		Set<Chunk> set = chunking.chunkSet();
		for (Chunk chunk : set)
		{
			System.out.println("Type: " + chunk.type() + " Entity: [" + text.substring(chunk.start(), chunk.end()) + "] Score " + chunk.score());
		}
	}
	
	public static void main(String[] args) {
		String sentences[] = {"Joe was the last person to see Fred. ", "He saw him in Boston at McKenzie's pub at 3:00 where he paid $2.45 for an ale. ", "Joe wanted to go to Vermont for the day to visit a cousin who works at IBM, but Sally and he had to look for Fred."};

		AbstractCharLmRescoringChunker chunker = null;
		try
		{
			File modelFile = new File("models", "ne-en-news-muc6.AbstractCharLmRescoringChunker");
			chunker = (AbstractCharLmRescoringChunker) AbstractExternalizable.readObject(modelFile);
		}
		catch (IOException | ClassNotFoundException ex)
		{
			System.out.print("File not fuound.");
			return;
		}
		
		for (String sentence : sentences)
		{
			displayChunkSet(chunker, sentence);
		}
	}

}