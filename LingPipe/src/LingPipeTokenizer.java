import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;
        import com.aliasi.tokenizer.Tokenizer;
        import java.util.*;

public class LingPipeTokenizer {

    public static void main(String[] args) {

        List<String> tokenList = new ArrayList<>();
        List<String> whiteList = new ArrayList<>();

        String text = "Amy didn’t like it. There were paintings of zombies and skeletons on the walls.";

        Tokenizer tokenizer = IndoEuropeanTokenizerFactory.INSTANCE.tokenizer(text.toCharArray(), 0, text.length());

        tokenizer.tokenize(tokenList, whiteList);

        for (String element : tokenList) {
            System.out.print(element + "\n");
        }
        System.out.println();
    }
}