import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;
import com.aliasi.tokenizer.Tokenizer;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<String> tokenList = new ArrayList<>();
        List<String> whiteList = new ArrayList<>();

        String text = "A sample sentence processed \nby \tthe " +
                "LingPipe tokenizer.";

        Tokenizer tokenizer = IndoEuropeanTokenizerFactory.INSTANCE.
                tokenizer(text.toCharArray(), 0, text.length());

        tokenizer.tokenize(tokenList, whiteList);

        for (String element : tokenList) {
            System.out.print(element + "\n");
        }
        System.out.println();




    }
}
