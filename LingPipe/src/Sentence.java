import com.aliasi.sentences.*;
import com.aliasi.tokenizer.*;
import java.util.*;

public class Sentence {

    static String text = "When determining the end of sentences we need to consider several factors. Sentences may end with exclamation marks! Or possibly questions marks? Within sentences we may find numbers like 3.14159, abbreviations such as found in Mr. Smith, and possibly ellipses either within a sentence …, or at the end of a sentence…";
    static String text2 = "“Will the prime minister listen to the will of house last night, end this costly charade, and rule out no deal?” the Labour leader asked. May responded by saying Corbyn was himself ruling out the only deal acceptable to Brussels. “The only way to avoid no deal is to vote for the deal,” she said.";

    public static void main(String[] args) {

        List<String> tokenList = new ArrayList<>();
        List<String> whiteList = new ArrayList<>();

        TokenizerFactory TOKENIZER_FACTORY = IndoEuropeanTokenizerFactory.INSTANCE;
        SentenceModel sentenceModel = new IndoEuropeanSentenceModel();

        Tokenizer tokenizer= TOKENIZER_FACTORY.tokenizer(text2.toCharArray(),0, text2.length());
        tokenizer.tokenize(tokenList, whiteList);

        String[] tokens = new String[tokenList.size()];
        String[] whites = new String[whiteList.size()];
        tokenList.toArray(tokens);
        whiteList.toArray(whites);

        int[] sentenceBoundaries = sentenceModel.boundaryIndices(tokens, whites);

        int start = 0;
        for(int boundary : sentenceBoundaries) {
            while(start<=boundary) {
                System.out.print(tokenList.get(start) + whiteList.get(start+1));
                start++;
            }
            System.out.println();
        }
    }
}