import java.io.*;

import com.aliasi.classify.*;
import com.aliasi.util.*;
// Analiza sentymentu na podstawie wcze�niej stworzonego modelu
public class SentimentAnalysis {
	
	public static void main(String[] args) {	
		String review = "An overly sentimental film with a somewhat problematic message, but its sweetness and charm are occasionally enough to approximate true depth and grace.";
		
		LMClassifier classifier = null;
		try
		{
			classifier = (LMClassifier) AbstractExternalizable.readObject(new File("models", "sentiment_analysis.model"));
		}
		catch (IOException | ClassNotFoundException ex)
		{
			System.out.print("File not found.");
			return;
		}
		
		Classification classification = classifier.classify(review);
		String bestCategory = classification.bestCategory();
		System.out.println(review);
		System.out.println("Best category: " + bestCategory);
	}
}