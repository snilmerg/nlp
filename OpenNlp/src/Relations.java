import opennlp.tools.chunker.*;
import opennlp.tools.cmdline.parser.ParserTool;
import opennlp.tools.parser.*;
import java.io.*;

public class Relations {

    public String Sentence;

    public Relations(String sentence){
        this.Sentence = sentence;
    }

    public static void main(String[] args) {

        String fileLocation = "en-parser-chunking.bin";
        try (InputStream modelInputStream = new FileInputStream(fileLocation)) {
            ParserModel model = new ParserModel(modelInputStream);
            Parser parser = ParserFactory.create(model);

            String sentence = "The cow jumped over the moon";
            Parse parses[] = ParserTool.parseLine(sentence, parser, 1);

            for(Parse parse : parses) {
                parse.show();
                parse.show();

//                String cos = parse.toString().replace('(');
//                parse.showCodeTree();
//                System.out.println("Probability: " + parse.getProb());
            }

        } catch (IOException ex) {
            System.out.println("Exceptionix");
        }

    }
}
