import opennlp.tools.chunker.*;
import opennlp.tools.postag.*;
import opennlp.tools.util.Span;
import java.io.*;
import java.util.*;

public class Chunker {

    private static String[] textArray = {"The", "voyage", "of", "the", "Abraham", "Lincoln", "was", "for", "a", "long", "time", "marked", "by", "no", "special", "incident."};
    private static String text = "The voyage of the Abraham Lincoln was for a long time marked by no special incident.";
    private static String[] TokenizedText = new String[textArray.length];

    public static void main(String[] args) {

        // tokenizacja textu do tokenizedtext
        Scanner scanner = new Scanner(text);
        int x=0;
        while(scanner.hasNext()) {
            TokenizedText[x] = scanner.next();
            x++;
        }

        for(String token : TokenizedText) {
            System.out.println(token);
        }

        // wykrywanie części mowy
        try (InputStream posModelStream = new FileInputStream("en-pos-maxent.bin");
             InputStream chunkerStream = new FileInputStream("en-chunker.bin");) {

            POSModel model = new POSModel(posModelStream);
            POSTaggerME tagger = new POSTaggerME(model);
            String tags[] = tagger.tag(TokenizedText);



            ChunkerModel chunkerModel = new ChunkerModel(chunkerStream);
            ChunkerME chunkerME = new ChunkerME(chunkerModel);
            String result[] = chunkerME.chunk(TokenizedText, tags);

            for (int i = 0; i < result.length; i++) {
                System.out.println("[" + TokenizedText[i] + "] " + result[i]);
            }


            Span[] spans = chunkerME.chunkAsSpans(TokenizedText, tags);
            for (Span span : spans) {
                System.out.print("Type: " + span.getType() + " - "
                        + " Begin: " + span.getStart()
                        + " End:" + span.getEnd()
                        + " Length: " + span.length() + "  [");
                for (int j = span.getStart(); j < span.getEnd(); j++) {
                    System.out.print(TokenizedText[j] + " ");
                }
                System.out.println("]");
            }



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
