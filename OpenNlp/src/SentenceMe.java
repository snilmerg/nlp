import opennlp.tools.sentdetect.*;
import opennlp.tools.util.Span;
import java.io.*;

public class SentenceMe {

    static String text = "When determining the end of sentences we need to consider several factors. Sentences may end with exclamation marks! Or possibly questions marks? Within sentences we may find numbers like 3.14159, abbreviations such as found in Mr. Smith, and possibly ellipses either within a sentence …, or at the end of a sentence…";
    static String text2 = "“Will the prime minister listen to the will of house last night, end this costly charade, and rule out no deal?” the Labour leader asked. May responded by saying Corbyn was himself ruling out the only deal acceptable to Brussels. “The only way to avoid no deal is to vote for the deal,” she said.";

    public static void main(String[] args) {



        try (InputStream is = new FileInputStream(new File("en-sent.bin"))) {
            SentenceModel model = new SentenceModel(is);
            SentenceDetectorME detector = new SentenceDetectorME(model);
            String sentences[] = detector.sentDetect(text);
            Span spans[] = detector.sentPosDetect(text);

            for (Span span : spans) {
                System.out.println(span + "[" + text.substring(span.getStart(), span.getEnd()) +"]");
            }
        } catch (FileNotFoundException ex) {
            // Handle exception
        } catch (IOException ex) {
            // Handle exception
        }
    }
}
