import java.io.*;
import java.util.*;

import opennlp.tools.namefind.*;
import opennlp.tools.tokenize.*;
import opennlp.tools.util.Span;
//znajdowanie byt�w nazwanych wykorzystuj�c modele: person, date, location, money, organization, time
public class NERModels {

    public static void main(String[] args) {
    	String sentences[] = {"Joe was the last person to see Fred. ", "He saw him in Boston at McKenzie's pub at 3:00 where he paid $2.45 for an ale. ", "Joe wanted to go to Vermont for the day to visit a cousin who works at IBM, but Sally and he had to look for Fred."};
    	
    	Tokenizer tokenizer = null;
    	NameFinderME nameFinder = null;
    	try
    	{
    		InputStream tokenStream = new FileInputStream(new File("models", "en-token.bin"));
    		TokenizerModel tokenModel = new TokenizerModel(tokenStream);
    		tokenizer = new TokenizerME(tokenModel);
    		
    		String modelNames[] = {"en-ner-person.bin", "en-ner-date.bin", "en-ner-location.bin", "en-ner-money.bin", "en-ner-organization.bin", "en-ner-time.bin"};
    		
    		for(String name : modelNames)
    		{
    			InputStream modelStream = new FileInputStream(new File("models", name));
        		TokenNameFinderModel entityModel = new TokenNameFinderModel(modelStream);
        		nameFinder = new NameFinderME(entityModel);
        		ArrayList<String> list = new ArrayList<String>();
        		
            	for(String sentence : sentences)
            	{
        	    	String tokens[] = tokenizer.tokenize(sentence);
        	    	Span nameSpans[] = nameFinder.find(tokens);
        	    	double spanProbs[] = nameFinder.probs(nameSpans);
        	    	for(int i = 0; i < nameSpans.length; i++)
        	    	{
        	    		list.add("Span: " + nameSpans[i].toString()+ "Entity: " + tokens[nameSpans[i].getStart()] + 
        	    			" Probability: " + spanProbs[i]);
        	    	}
            	}
            	
            	for(String element : list)
            	{
            		System.out.println(element);
            	}
    		}
    	}
    	catch(IOException e)
    	{
    		// Handle Exception
    	}
    }
}