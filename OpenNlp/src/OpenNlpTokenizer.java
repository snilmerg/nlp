import java.io.*;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

public class OpenNlpTokenizer {

    static String text = "Amy didn’t like it. There were paintings of zombies and skeletons on the walls.";

    public static void main(String[] args) {
        InputStream modelIn = null;

        try {
            modelIn = new FileInputStream("en-token.bin");
            TokenizerModel model = new TokenizerModel(modelIn);
            TokenizerME tokenizer = new TokenizerME(model);
            String tokens[] = tokenizer.tokenize(text);

            for (String a : tokens) {
            System.out.println(a);
            }

        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (modelIn != null) {
                try {
                    modelIn.close();
                }
                catch (IOException e) {
                }
            }
        }
    }
}