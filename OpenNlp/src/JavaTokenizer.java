import java.util.*;

public class JavaTokenizer {

    static String text = "Amy didn’t like it. There were paintings of zombies and skeletons on the walls.";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(text);

        List<String> list = new ArrayList<>();
        while(scanner.hasNext()) {
            String token = scanner.next();
            list.add(token);
        }

        for(String token : list) {
            System.out.println(token);
        }
    }
}
