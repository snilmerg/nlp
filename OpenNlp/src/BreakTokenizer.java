import java.text.BreakIterator;
import java.util.*;

public class BreakTokenizer {

    static String text = "Amy didn’t like it. There were paintings of zombies and skeletons on the walls.";

    public static void main(String[] args) {
        BreakIterator wordIterator = BreakIterator.getWordInstance();

        wordIterator.setText(text);
        int boundary = wordIterator.first();

        while (boundary != BreakIterator.DONE) {
            int begin = boundary;
            System.out.print(boundary + "-");
            boundary = wordIterator.next();
            int end = boundary;
            if(end == BreakIterator.DONE) break;
            System.out.println(boundary + " ["
                    + text.substring(begin, end) + "]");
        }
    }
}
