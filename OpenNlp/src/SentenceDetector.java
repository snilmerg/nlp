import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;

/**
 * Sentence Detection Example in openNLP using Java
 * @author tutorialkart
 */
public class SentenceDetector {

    public static void main(String[] args) {
        try {
            new SentenceDetector().sentenceDetect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is used to detect sentences in a paragraph/string
     * @throws InvalidFormatException
     * @throws IOException
     */
    public void sentenceDetect() throws InvalidFormatException,	IOException {
        String paragraph = "Christmas is a celebration that lasts for several days, 'Most wonderful time of the year.' someone could say. In the UK and many other countries, the main celebration takes place on Christmas Day (25 December). From the Christian origins of the holiday, this day marks the birth of Jesus Christ. Christmas Eve (24 December) is the time for last-minute shopping and preparations, present-wrapping and maybe a drink in the pub. Others will be at home preparing food for the big day or at a midnight church service to welcome Christmas Day. Boxing Day (26 December) is also a national holiday in the UK – a necessary one for many, to recover after eating too much the day before! Shops are usually open on Boxing Day and the big after-Christmas sales begin.";

        // refer to model file "en-sent,bin", available at link http://opennlp.sourceforge.net/models-1.5/
        InputStream is = new FileInputStream("en-sent.bin");
        SentenceModel model = new SentenceModel(is);

        // feed the model to SentenceDetectorME class
        SentenceDetectorME sdetector = new SentenceDetectorME(model);

        // detect sentences in the paragraph
        String sentences[] = sdetector.sentDetect(paragraph);

        // print the sentences detected, to console
        for(int i=0;i<sentences.length;i++){
            System.out.println("Sentence " + i + ": " + sentences[i]);
        }
        is.close();
    }
}